package com.example.mymeditation

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.mymeditation.databinding.ActivityOnboardingBinding

class OnboardingActivity : AppCompatActivity() {

    private lateinit var binding: ActivityOnboardingBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)



        binding = ActivityOnboardingBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.enterAccountBottom.setOnClickListener {
            startActivity(
               Intent(this, LoginActivity::class.java)
            )

        }

        binding.registrationText.setOnClickListener {
            startActivity(
                Intent(this, RegisterActivity :: class.java)
            )

        }





    }
}