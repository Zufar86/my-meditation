package com.example.mymeditation

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.mymeditation.databinding.ActivityLoginBinding
import com.example.mymeditation.databinding.ActivityOnboardingBinding

class LoginActivity : AppCompatActivity() {

    private lateinit var binding: ActivityLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.singInBottom.setOnClickListener {
            val email = binding.emailEntry.text.toString()
            val password = binding.passwordEntry.text.toString()

            if (email.isEmpty())
                binding.emailEntry.error = "Введите Email"

            else if (password.isEmpty())
                binding.passwordEntry.error = "Введите пароль"

            else{
            startActivity(
                Intent (this, MainActivity::class.java)

            )
            finish()}
        }
        binding.registerButon.setOnClickListener {
            startActivity(
                Intent(this, RegisterActivity::class.java)
            )
        }

    }
}